# transmatrix xscreensaver

Working on integrating this more directly with xscreensaver:
Having options to turn on/off transflag or rainbow flag colours (otherwise keeping the original green font)

At the moment, this can be installed by dropping hacks/ directly into the xscreensaver source directory:
xmatrix.c would then replace the original (green will be swapped for trans flag colours)

Then build as usual with:

./configure; make; sudo make install

--trans
	the matrix screensaver will now have trans colours
--rainbow
	the matrix screensaver will now have rainbow colours
	
This was basically done by Raina Matthews, I take no credit for what little I contributed.